﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicFlee : BaseBehavior
{
    public Transform target;
    public float speed = 10;

    public override Vector3 GetVelocity()
    {
        if (target == null)
        {
            return Vector3.zero;
        }

        Vector3 ret = transform.position - target.position;
        ret.y = 0;

        return ret.normalized * speed;
    }

    public override float GetRotation()
    {
        if (target == null)
        {
            return 0;
        }

        Vector3 dir = (transform.position - target.position); dir.y = 0;
        return SignedAngle(transform.forward, dir.normalized) / Time.deltaTime; //Vector3.Angle(transform.forward, dir.normalized);
    }

    public static float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }

}
