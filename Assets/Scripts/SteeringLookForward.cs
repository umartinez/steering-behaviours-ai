﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringLookForward : SteeringAlign
{
    public override float GetAngularAcceleration()
    {
        var npc = GetComponent<NPC>();
        var rotation = SignedAngle(npc.transform.forward, npc.CurrentVelocity.normalized);

        return GetTargetRotation(rotation);
    }
}
