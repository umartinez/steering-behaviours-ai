﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringArrive : BaseBehavior
{
    public Transform target;

    public float maxAcceleration = 100;
    public float targetRadius = 0.25f;
    public float slowRadius = 2;
    public float timeToTarget = 0.1f;

    public bool stopOnArrive = true;

    private bool shouldMove = true;

    public override Vector3 GetAcceleration()
    {
        if (target == null)
        {
            return Vector3.zero;
        }

        var npc = GetComponent<NPC>();

        Vector3 distance = target.position - transform.position;
        distance.y = 0;
        // Debug.Log("[SteeringArrive] Distance:" + distance);

        shouldMove = true;
        if (distance.sqrMagnitude < targetRadius*targetRadius)
        {
            // shouldMove = false;
            // return Vector3.zero;
        }

        var targetSpeed = 0.0f;
        if (distance.sqrMagnitude > slowRadius*slowRadius)
        {
            // distance = distance.normalized * maxSpeed;
            targetSpeed = npc.maxSpeed;
        }
        else
        {
            // distance = distance * maxSpeed / slowRadius;
            targetSpeed = npc.maxSpeed * distance.magnitude / slowRadius;
        }

        var targetVelocity = distance.normalized * targetSpeed;
        var ret = (targetVelocity - npc.CurrentVelocity) / timeToTarget;
        if (ret.sqrMagnitude > maxAcceleration*maxAcceleration)
        {
            ret = ret.normalized * maxAcceleration;
        }
        return ret;
    }

    /*
    public override float GetRotation()
    {
        if (target == null)
        {
            return 0;
        }

        Vector3 dir = (target.position - transform.position); dir.y = 0;
        var angle = SignedAngle(transform.forward, dir.normalized);
        return angle*0.5f / Time.deltaTime; // / Time.deltaTime; //Vector3.Angle(transform.forward, dir.normalized);
    }
    */

    public static float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }

    public override bool ShouldMove()
    {
        return shouldMove || !stopOnArrive;
    }

}
