﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public float maxSpeed = 10.0f;
    public float maxAngularSpeed = 10.0f;

    Vector3 currentVelocity = Vector3.zero;
    float currentAngularVelocity = 0;

    public Vector3 CurrentVelocity
    {
        get
        {
            return currentVelocity;
        }
    }

    public float CurrentAngularVelocity
    {
        get
        {
            return currentAngularVelocity;
        }

        set
        {
            currentAngularVelocity = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        var behaviours = GetComponents<BaseBehavior>();
        foreach (var b in behaviours)
        {
            if (b.enabled)
            {
                transform.position += b.GetVelocity() * Time.deltaTime;
                transform.rotation = Quaternion.Euler(0, b.GetRotation() * Time.deltaTime, 0) * transform.rotation;

                currentVelocity += b.GetAcceleration() * Time.deltaTime;
                currentAngularVelocity += b.GetAngularAcceleration() * Time.deltaTime;

                if (!b.ShouldMove())
                {
                    currentVelocity = Vector3.zero;
                }
            }
        }

        if (currentVelocity.sqrMagnitude > maxSpeed * maxSpeed)
        {
            currentVelocity = currentVelocity.normalized * maxSpeed;
        }
        if (Mathf.Abs(currentAngularVelocity) > maxAngularSpeed)
        {
            currentAngularVelocity = maxAngularSpeed * Mathf.Sign(currentAngularVelocity);
        }

        transform.position += currentVelocity * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, currentAngularVelocity * Time.deltaTime, 0) * transform.rotation;
    }
}
