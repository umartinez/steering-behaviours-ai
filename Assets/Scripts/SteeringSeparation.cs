﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringSeparation : BaseBehavior
{
    public float maxAcceleration = 100;

    public Transform[] targets;

    public float threshold = 5.0f;
    public float decayCoef = 1.0f;

    public override Vector3 GetAcceleration()
    {
        var ret = Vector3.zero;
        var npc = GetComponent<NPC>();

        var thresholdSquared = threshold * threshold;
        foreach (var t in targets)
        {
            var direction = t.position - transform.position;
            direction.y = 0;

            var distance = direction.sqrMagnitude;

            if (distance < thresholdSquared)
            {
                var strength = -Mathf.Min(decayCoef * distance, maxAcceleration);
                ret += strength * direction.normalized;
            }
        }

        return ret;
    }
}
