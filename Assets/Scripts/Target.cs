﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float speed;
    public float angularSpeed = 100;

		// Update is called once per frame
	void Update ()
    {
        var hztal = Input.GetAxis("Horizontal");
        var vtcal = Input.GetAxis("Vertical");

        var movementDir = Camera.main.transform.TransformDirection(hztal, 0, vtcal);
        movementDir.y = 0;

        transform.position += movementDir.normalized * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(0, -angularSpeed * Time.deltaTime, 0);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(0, angularSpeed * Time.deltaTime, 0);
        }
    }
}
