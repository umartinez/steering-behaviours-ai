﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicArrive : BaseBehavior {

    public Transform target;
    public float speed = 10;
    public float arrivingRadius = 2;
    public float timeToArrive = 0.25f;

    public override Vector3 GetVelocity()
    {
        if (target == null)
        {
            return Vector3.zero;
        }

        Vector3 ret = target.position - transform.position;
        ret.y = 0;

        if (ret.sqrMagnitude > arrivingRadius*arrivingRadius)
        {
            return ret.normalized * speed;
        }

        ret /= timeToArrive;
        if (ret.sqrMagnitude > speed*speed)
        {
            return ret.normalized * speed;
        }

        return ret;
    }

    public override float GetRotation()
    {
        if (target == null)
        {
            return 0;
        }

        Vector3 dir = (target.position - transform.position); dir.y = 0;
        return SignedAngle(transform.forward, dir.normalized) / Time.deltaTime; //Vector3.Angle(transform.forward, dir.normalized);
    }

    public static float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }
}
