﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringAlign : BaseBehavior
{
    public Transform target;

    public float maxRotation = 100;
    public float targetRadius = 0.25f;
    public float slowRadius = 2;
    public float timeToTarget = 0.1f;

    public override float GetAngularAcceleration()
    {
        var rotation = SignedAngle(transform.forward, target.forward);

        return GetTargetRotation(rotation);
    }

    protected float GetTargetRotation(float rotation)
    {
        var npc = GetComponent<NPC>();
        var targetRotation = 0.0f;
        var rotAbs = Mathf.Abs(rotation);

        if (rotAbs < targetRadius)
        {
            // return 0;
        }

        if (rotAbs > slowRadius)
        {
            targetRotation = maxRotation;
        }
        else
        {
            targetRotation = maxRotation * rotAbs / slowRadius;
        }

        targetRotation *= Mathf.Sign(rotation);

        var ret = targetRotation - npc.CurrentAngularVelocity;
        ret /= timeToTarget;

        if (Mathf.Abs(ret) > npc.maxAngularSpeed)
        {
			ret = Mathf.Sign(ret) * maxRotation;
        }

        return ret;
    }

    public static float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }
}
