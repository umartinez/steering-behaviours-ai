﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicWander : BaseBehavior
{
    public float speed = 10;
    public float maxRotation = 45;

    public override Vector3 GetVelocity()
    {
        return transform.forward * speed;
    }

    public override float GetRotation()
    {
        return (Random.value - Random.value) * maxRotation;
    }
}
