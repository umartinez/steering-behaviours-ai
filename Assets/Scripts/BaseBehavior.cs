﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBehavior : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        OnStart();
	}

    virtual protected void OnStart()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        OnUpdate();
	}

    virtual protected void OnUpdate()
    {

    }

    public virtual Vector3 GetVelocity()
    {
        return Vector3.zero;
    }

    public virtual float GetRotation()
    {
        return 0;
    }

    public virtual Vector3 GetAcceleration()
    {
        return Vector3.zero;
    }

    public virtual float GetAngularAcceleration()
    {
        return 0;
    }

    public virtual bool ShouldMove()
    {
        return true;
    }
}
