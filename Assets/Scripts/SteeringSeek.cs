﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringSeek : BaseBehavior
{
    public Transform target;
    public float acceleration;

    public override Vector3 GetAcceleration()
    {
        if (target == null)
        {
            return Vector3.zero;
        }

        return GetAccelerationFor(target.position);
    }

    protected virtual Vector3 GetAccelerationFor(Vector3 targetPos)
    {
        var ret = targetPos - transform.position;
        ret.y = 0;

        return ret.normalized * acceleration;
    }
    
    /*
    public override float GetRotation()
    {
        if (target == null)
        {
            return 0;
        }

        Vector3 dir = (target.position - transform.position); dir.y = 0;
        return SignedAngle(transform.forward, dir.normalized) / Time.deltaTime; //Vector3.Angle(transform.forward, dir.normalized);
    }

    public static float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }
    */
}
