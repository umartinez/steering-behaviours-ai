﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringObstaclesAvoidance : SteeringSeek
{
    public float avoidDistance = 10.0f;
    public float rayLength = 5.0f;
    public LayerMask obstaclesMask;

    private bool hitDone = false;
    private Vector3 avoidPosition = Vector3.zero;

    public override Vector3 GetAcceleration()
    {
        var npc = GetComponent<NPC>();

        hitDone = false;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, npc.CurrentVelocity.normalized, out hit, rayLength, obstaclesMask))
        {
            var targetPos = hit.point + hit.normal * avoidDistance;
            targetPos.y = transform.position.y;

            hitDone = true;
            avoidPosition = targetPos;

            return GetAccelerationFor(targetPos);
        }

        return Vector3.zero;
    }

    void OnDrawGizmos()
    {
        var npc = GetComponent<NPC>();

        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, npc.CurrentVelocity.normalized * rayLength);

        if (hitDone)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position + Vector3.up * 2.0f, 0.5f);
            Gizmos.DrawLine(transform.position, avoidPosition);
        }
    }
}
